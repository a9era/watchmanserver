FROM python:2.7.15-alpine

RUN apk update \
    && apk add libpq postgresql-dev \
    && apk add build-base

WORKDIR /app
ADD . /app

RUN pip install -r requirement.txt


EXPOSE 5000

CMD [ "python", "./app.py" ]