from flask import Flask, jsonify
from flask_socketio import SocketIO, send, emit
import connection
import datetime

async_mode = None
params = {
    'ping_timeout': 35
}

app = Flask(__name__)
app.config['SECRET_KEY'] = 'mysecret'
socketio = SocketIO(app, async_mode=async_mode, **params)

@app.route('/')
def index():
    return 'Hello World2'

# DB connection -------------------------------
CONN = None
CUR = None

# COOKIE
DATA = None

def getConnCur():
    global CONN
    global CUR
    return CONN, CUR

def OpenConn():
    global CONN
    global CUR
    print '[!] Connecting to database . . .'
    CONN, CUR = connection.Connect()

def CloseConn():
    global CONN
    global CUR
    print '[!] Closeing database . . .'
    if CONN:
        connection.Close(CONN, CUR)
# ---------------------------------------------

# SOCKET IO ---------------------------------------------------------------
# RECIVE -------------------------------
# @socketio.on('userJoined')
# def handleMessage(msg):
#     print 'Message: {}'.format(msg)
#     send(msg, broadcast=True)
# SEND ---------------------------------

@socketio.on('connect')
def handleMessage():
    print '[+] a User connected'
    socketio.emit('confirmConnect', 'Server connected', broadcast=True)

@socketio.on('chat message')
def chatMsg(msg):
    print msg

@socketio.on('btn change')
def btnChange(data):
    loggingRequrest('SOCKET < btn change',data)
    socketio.emit('btnChanged', data, broadcast=True)
    loggingRequrest('SOCKET > btnChanged',data)

@socketio.on('btnSet')
def btnSet(data):
    loggingRequrest('SOCKET < btnSet',data)
    
    if data == 'atHome':
        sendSignal(room='livingroom',device='light1',value='1')
        sendSignal(room='kitchen',device='light1',value='1')
        updateMode(device='motion',mode='3')
        updateMode(device='flame',mode='2')
        updateMode(device='smoke',mode='2')
    elif data == 'outDoor':
        controlAllDevices(value='0')
        updateMode(device='motion',mode='1')
        updateMode(device='flame',mode='1')
        updateMode(device='smoke',mode='1')
    elif data == 'sleep':
        controlAllDevices(value='0')
        socketio.emit('locationModeChanged', 'off', broadcast=True)
        loggingRequrest('SOCKET > locationModeChanged',data)

@socketio.on('setHomeLocation')
def setHomeLocation(data):
    loggingRequrest('SOCKET < setHomeLocation',data)
    home_lat = data['latitude']
    home_lng = data['longitude']
    conn, cur = getConnCur()
    try:
        connection.setShareVariable(conn, cur, 'home_lat',home_lat)
        connection.setShareVariable(conn, cur, 'home_lng',home_lng)
        socketio.emit('homeLocationChanged', {'latitude':float(home_lat),'longitude':float(home_lng)}, broadcast=True)
        loggingRequrest('SOCKET > homeLocationChanged',{'latitude':float(home_lat),'longitude':float(home_lng)})
    except Exception as err:
        OpenConn()
        conn, cur = getConnCur()
        connection.setShareVariable(conn, cur, 'home_lat',home_lat)
        connection.setShareVariable(conn, cur, 'home_lng',home_lng)
        socketio.emit('homeLocationChanged', {'latitude':float(home_lat),'longitude':float(home_lng)}, broadcast=True)
        loggingRequrest('SOCKET > homeLocationChanged',{'latitude':float(home_lat),'longitude':float(home_lng)})

@socketio.on('getHomeLocation')
def getHomeLocation(msg):
    loggingRequrest('SOCKET < getHomeLocation','')
    home_lat = ''
    home_lng = ''
    conn, cur = getConnCur()
    try:
        home_lat = connection.getShareVariable(conn, cur, 'home_lat')
        home_lng = connection.getShareVariable(conn, cur, 'home_lng')
    except Exception as err:
        OpenConn()
        conn, cur = getConnCur()
        home_lat = connection.getShareVariable(conn, cur, 'home_lat')
        home_lng = connection.getShareVariable(conn, cur, 'home_lng')
    socketio.emit('getHomelocationFromDB', {'latitude':float(home_lat),'longitude':float(home_lng)}, broadcast=True)
    loggingRequrest('SOCKET > getHomelocationFromDB',{'latitude':home_lat,'longitude':home_lng})

@socketio.on('locationModeChange')
def locationModeChange(data):
    loggingRequrest('SOCKET < locationModeChange',data)
    socketio.emit('locationModeChanged', data, broadcast=True)
    loggingRequrest('SOCKET > locationModeChanged',data)

# END SOCKET IO -----------------------------------------------------------


@app.route('/sendSignal/<room>/<device>/<value>', methods=['GET', 'POST'])
def sendSignal(room=None,device=None,value=None):
    loggingRequrest('GET','/sendSignal/{}/{}/{}'.format(room, device, value))
    conn, cur = getConnCur()
    try:
        connection.addSignal(conn, cur, room, device, value)
    except Exception as err:
        OpenConn()
        conn, cur = getConnCur()
        connection.addSignal(conn, cur, room, device, value)
    socketio.emit('deviceToggle', {'room':room,'device':device,'value':value}, broadcast=True)
    return '[!] {}:{} status: {}  sended !'.format(room,device,value)
    
@app.route('/controlAllDevices/<value>', methods=['GET', 'POST'])
def controlAllDevices(value=None):
    loggingRequrest('GET','/controlAllDevices/{}'.format(value))
    conn, cur = getConnCur()
    try:
        connection.addSignal(conn, cur, 'livingroom', 'light1', value)
        connection.addSignal(conn, cur, 'kitchen', 'light1', value)
        connection.addSignal(conn, cur, 'bedroom', 'light1', value)
        connection.addSignal(conn, cur, 'bedroom2', 'light1', value)
    except Exception as err:
        OpenConn()
        conn, cur = getConnCur()
        connection.addSignal(conn, cur, 'livingroom', 'light1', value)
        connection.addSignal(conn, cur, 'kitchen', 'light1', value)
        connection.addSignal(conn, cur, 'bedroom', 'light1', value)
        connection.addSignal(conn, cur, 'bedroom2', 'light1', value)
    socketio.emit('deviceToggle', {'room':'ALL','value':value}, broadcast=True)
    return 'ALL > {}'.format(value)

@app.route('/sendSensorSignal/<room>/<device>/<isalert>/<description>', methods=['GET', 'POST'])
def sendSensorSignal(room=None,device=None,isalert=None,description=None):
    loggingRequrest('GET','/sendSensorSignal/{}/{}/{}/{}'.format(room, device, isalert, description))
    conn, cur = getConnCur()
    try:
        connection.addSensorSignal(conn, cur, room, device, isalert, description)
    except Exception as err:
        OpenConn()
        conn, cur = getConnCur()
        connection.addSensorSignal(conn, cur, room, device, isalert, description)
    socketio.emit('sensorAlert', 'Detected', broadcast=True)
    loggingRequrest('SOCKET > sensorAlert','Detected')
    socketio.emit('Last detection update', {'device':device}, broadcast=True)
    loggingRequrest('SOCKET > Last detection update',{'device':device})
    return '[!] {}:{} isalert: {} | description:{}  sended !'.format(room,device,isalert,description)

@app.route('/checkalerted/<room>/<device>', methods=['GET', 'POST'])
def checkalerted(room=None,device=None):
    loggingRequrest('GET','/checkalerted/{}/{}'.format(room, device))
    conn, cur = getConnCur()
    try:
        connection.checkAlerted(conn, cur, room, device)
    except Exception as err:
        OpenConn()
        conn, cur = getConnCur()
        connection.checkAlerted(conn, cur, room, device)
    return 'Checked'

@app.route('/updateTemp/<room>/<temp>/<humidity>', methods=['GET', 'POST'])
def updateTemp(room=None,temp=None,humidity=None):
    loggingRequrest('GET','/updateTemp/{}/{}/{}'.format(room, temp, humidity))
    conn, cur = getConnCur()
    try:
        connection.updateTemp(conn, cur, room, temp, humidity)
    except Exception as err:
        OpenConn()
        conn, cur = getConnCur()
        connection.updateTemp(conn, cur, room, temp, humidity)
    socketio.emit('tempUpdated', {'room':room,'temp':temp,'humidity':humidity}, broadcast=True)
    return 'Updated'

@app.route('/updateMode/<device>/<mode>', methods=['GET', 'POST'])
def updateMode(device=None,mode=None):
    loggingRequrest('GET','/updateMode/{}/{}'.format(device, mode))
    conn, cur = getConnCur()
    try:
        connection.updateSensorMode(conn, cur, device, mode)
    except Exception as err:
        OpenConn()
        conn, cur = getConnCur()
        connection.updateSensorMode(conn, cur, device, mode)
    socketio.emit('Mode Updated', {'device':device,'mode':int(mode)}, broadcast=True)
    return 'Updated'


@app.route('/getTempStatus/<room>', methods=['GET'])
def getTempStatus(room=None):
    loggingRequrest('GET','/getTempStatus/{}'.format(room))
    d = []
    conn, cur = getConnCur()
    try:
        d = connection.fetchTempStatus(conn, cur, room)
    except Exception as err:
        OpenConn()
        conn, cur = getConnCur()
        d = connection.fetchTempStatus(conn, cur, room)
    return jsonify(d)

@app.route('/getDeviceStatus', methods=['GET'])
def getDeviceStatus():
    loggingRequrest('GET','/getDeviceStatus')
    global DATA
    conn, cur = getConnCur()
    try:
        d = connection.fetchDeviceStatus(conn, cur)
    except Exception as err:
        OpenConn()
        conn, cur = getConnCur()
        d = connection.fetchDeviceStatus(conn, cur)

    if d == None:
        return jsonify(DATA)
    else:
        DATA = d
        return jsonify(d)

@app.route('/getDeviceValue/<room>/<device>', methods=['GET'])
def getDeviceValue(room=None,device=None):
    loggingRequrest('GET','/getDeviceValue/{}/{}'.format(room, device))
    conn, cur = getConnCur()
    try:
        d = connection.getDeviceStatus(conn, cur, room, device)
    except Exception as err:
        OpenConn()
        conn, cur = getConnCur()
        d = connection.getDeviceStatus(conn, cur, room, device)

    return jsonify(d)
    
@app.route('/getLastDetecte/<device>', methods=['GET'])
def getLastDetecte(device=None):
    loggingRequrest('GET','/getLastDetecte/{}'.format(device))
    conn, cur = getConnCur()
    try:
        d = connection.getLastDetected(conn, cur, device)
    except Exception as err:
        OpenConn()
        conn, cur = getConnCur()
        d = connection.getLastDetected(conn, cur, device)

    return jsonify(d)

@app.route('/getHistoryDetectec/<device>', methods=['GET'])
def getHistoryDetectec(device=None):
    loggingRequrest('GET','/getHistoryDetectec/{}'.format(device))
    conn, cur = getConnCur()
    try:
        d = connection.getHistoryDetected(conn, cur, device)
    except Exception as err:
        OpenConn()
        conn, cur = getConnCur()
        d = connection.getHistoryDetected(conn, cur, device)

    return jsonify(d)

@app.route('/getSensorStatus', methods=['GET'])
def getSensorStatus():
    loggingRequrest('GET','/getSensorStatus/')
    conn, cur = getConnCur()
    d = []
    try:
        d = connection.fetchSensorStatus(conn, cur)
    except Exception as err:
        OpenConn()
        conn, cur = getConnCur()
        d = connection.fetchSensorStatus(conn, cur)
    
    return jsonify(d)

def loggingRequrest(method,path):
    print '[{}] >> {} || {}'.format(method, path, datetime.datetime.now())

if __name__ == '__main__':
    try:
        OpenConn()
        # app.run(host='0.0.0.0', port=5000, debug=False)
        print '[/] Server running on PORT:5000'
        socketio.run(app, host='0.0.0.0', port=5000)
    except Exception as err:
        print err
    finally:
        print '[!] Server has been shut down'
        CloseConn()

# pg_ctl -D "C:\Program Files\PostgreSQL\10\data" start
