import psycopg2

def Connect():
    try:
        conn = psycopg2.connect(dbname='IOT', user='postgres', host='localhost', password='1234') 
        cur = conn.cursor()
        print '[/] Connected !!'
    except Exception as err:
        print err
    return conn, cur


def Close(conn, cur):
    cur.close()
    conn.close()
    print '[/] Closed !!'



def addSignal(conn, cur, room, device, value):
    try:
        
        sql = """UPDATE control_live_data SET value='{}' WHERE room='{}' AND device='{}'""".format(value ,room, device)
        cur.execute(sql)
        conn.commit()
        
    except Exception as err:
        print err
        conn.rollback()

def addSensorSignal(conn, cur, room, device, isalert, description):
    try:
        
        sql1 = """UPDATE sensor_signal SET isalert='{}' , description='{}' WHERE room='{}' AND device='{}'""".format(isalert, description, room, device)
        cur.execute(sql1)
        conn.commit()
        
        sql2 = """insert into history_{} (room, time_detected) values ('{}', current_timestamp)""".format(device, room)
        cur.execute(sql2)
        conn.commit()
    except Exception as err:
        print err
        conn.rollback()


def fetchDeviceStatus(conn, cur):
    try:

        data = []
        sql = """SELECT room,device,value FROM control_live_data"""
        cur.execute(sql)
        result = cur.fetchall()
        for r in result:
            e = {
                'room':r[0],
                'device':r[1],
                'value':r[2]
            }
            data.append(e)
        return data
    except Exception as err:
        print err
        conn.rollback()

def getDeviceStatus(conn, cur, room, device):
    try:

        data = []
        sql = """SELECT value FROM control_live_data where room = '{}' and device = '{}'""".format(room, device)
        cur.execute(sql)
        result = cur.fetchone()
        if result:
            e = {
                'value':result[0]
            }
            data.append(e)
        return data
    except Exception as err:
        print err
        conn.rollback()

def fetchSensorStatus(conn, cur):
    try:
        data = []
        sql = """SELECT room, device, isalert, description, mode FROM sensor_signal """
        cur.execute(sql)
        result = cur.fetchall()
        for r in result:
            e = {
                'room':r[0],
                'device':r[1],
                'isalert':r[2],
                'description':r[3],
                'mode':r[4]
            }
            data.append(e)
        return data
    except Exception as err:
        print err
        conn.rollback()

def checkAlerted(conn, cur, room, device):
    try:
        sql = """UPDATE sensor_signal SET isalert='0' , description='0' WHERE room='{}' AND device='{}'""".format(room, device)
        cur.execute(sql)
        conn.commit()
    except Exception as err:
        print err
        conn.rollback()



def updateTemp(conn, cur, room, temp, humidity):
    try:
        sql = """UPDATE temperature_status SET temp='{}' , humidity='{}' WHERE room='{}'""".format(temp, humidity, room)
        cur.execute(sql)
        conn.commit()
    except Exception as err:
        print err
        conn.rollback()

def updateSensorMode(conn, cur, device, mode):
    try:
        sql = """UPDATE sensor_signal SET mode='{}' WHERE device='{}'""".format(mode, device)
        cur.execute(sql)
        conn.commit()
    except Exception as err:
        print err
        conn.rollback()

def fetchTempStatus(conn, cur, room):
    try:
        data = []
        sql = """SELECT * FROM temperature_status where room='{}'""".format(room)
        cur.execute(sql)
        result = cur.fetchall()
        for r in result:
            e = {
                'room':r[1],
                'temp':r[2],
                'humidity':r[3]
            }
            data.append(e)
        return data
    except Exception as err:
        print err
        conn.rollback()

def getLastDetected(conn, cur, device):
    try:
        data = []
        if device == 'motion':
            sql = """SELECT time_detected FROM history_motion ORDER BY time_detected DESC LIMIT 1"""
        elif device == 'flame':
            sql = """SELECT time_detected FROM history_flame ORDER BY time_detected DESC LIMIT 1"""
        elif device == 'smoke':
            sql = """SELECT time_detected FROM history_smoke ORDER BY time_detected DESC LIMIT 1"""
        cur.execute(sql)
        result = cur.fetchall()
        for r in result:
            e = {
                'time':r[0]
            }
            data.append(e)
        return data
    except Exception as err:
        print err
        conn.rollback()

def getHistoryDetected(conn, cur, device):
    try:
        data = []
        if device == 'motion':
            sql = """SELECT time_detected FROM history_motion ORDER BY time_detected DESC"""
        elif device == 'flame':
            sql = """SELECT time_detected FROM history_flame ORDER BY time_detected DESC"""
        elif device == 'smoke':
            sql = """SELECT time_detected FROM history_smoke ORDER BY time_detected DESC"""
        cur.execute(sql)
        result = cur.fetchall()
        for r in result:
            e = {
                'time':r[0]
            }
            data.append(e)
        return data
    except Exception as err:
        print err
        conn.rollback()

def setShareVariable(conn, cur, var_name, var_value):
    try:
        sql = """UPDATE share_variable SET var_value='{}' WHERE var_name='{}'""".format(var_value, var_name)
        cur.execute(sql)
        conn.commit()
    except Exception as err:
        print err
        conn.rollback()
        
def getShareVariable(conn, cur, var_name):
    try:
        data = ''
        sql = """SELECT var_value FROM share_variable WHERE var_name = '{}'""".format(var_name)
        cur.execute(sql)
        result = cur.fetchall()
        for r in result:
            data = r[0]
        return data
    except Exception as err:
        print err