psycopg2
click==6.6
eventlet==0.24.1
Flask
flask-socketio
Flask-WTF==0.9.5
greenlet==0.4.15
itsdangerous==1.1.0
Jinja2==2.10
MarkupSafe==1.1.0
Werkzeug==0.14.1
WTForms==1.0.5